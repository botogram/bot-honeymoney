f = (...args) => console.log(...args)

const config = require('./config')
const Config = new config()


const _ = require('lodash')
const I18n = require('telegraf-i18n')
const path = require('path')
const Telegraf = require('telegraf')
const TelegrafFlow = require('telegraf-flow')
const request = require('request-promise')

const {Scene, WizardScene, enter} = TelegrafFlow
const {Extra, Markup} = Telegraf

const RedisSession = require('telegraf-session-redis')


const Info = require('./middlewares/info')


const info = new Info({
  directory: path.resolve(__dirname, 'info')
})

const bot = new Telegraf(Config.bot.token)

bot.use((ctx, next) => {
  const start = new Date()
  return next().then(() => {
    const ms = new Date() - start
    console.log('response time %s - %s - %s', ms, ctx.from.first_name, ctx.updateType)
  })
})


const session = new RedisSession({
  store: {
    host: '127.0.0.1',
    port: 6379,
    db: 1
  }
})


i18n = new I18n({
  defaultLocale: 'ru',
  directory: path.resolve(__dirname, 'locales')
})

bot.use(session.middleware())
bot.use(i18n.middleware())
bot.use(info.middleware())


flow = new TelegrafFlow()

flow.command('start', (ctx) => {
  return ctx.flow.enter('wizard')
})


const wizard = new WizardScene('wizard',
  (ctx) => {
    return request('https://api.coindesk.com/v1/bpi/currentprice/RUB.json').then((result) => {
      try {
        result = JSON.parse(result)
      } catch (e) {
        ctx.reply('Error get currency')
      }
      const price = (result.bpi.RUB.rate_float * 1.1).toFixed(0)
      ctx.session.price = price
      ctx.flow.wizard.next()
      return ctx.reply(ctx.i18n.t('welcome', {currency: price}), Markup.hideKeyboard().extra())
    })
  },
  (ctx) => {
    if (ctx.message.text && +ctx.message.text) {
      const price = (1 / ctx.session.price * ctx.message.text).toFixed(4)
      ctx.session.price = price
      ctx.flow.wizard.next()
      return ctx.reply(ctx.i18n.t('price', {price: price}))
    }
  },
  (ctx) => {
    f(ctx.session)
    if (ctx.message.text) {
      ctx.session.wallet = ctx.message.text
      ctx.flow.wizard.next()
      return ctx.reply(ctx.i18n.t('bank'), Markup
        .keyboard(_.chunk(_.keys(ctx.info.banks), 1))
        .extra()
      )
    }
  },
  (ctx) => {
    if (ctx.info.banks[ctx.message.text]) {
      ctx.session.bank = ctx.info.banks[ctx.message.text]
      ctx.session.id = Math.floor(Math.random() * 9999999) + 1
      ctx.flow.leave()
      return ctx.reply(ctx.i18n.t('ok', {
        bank: ctx.info.banks[ctx.message.text],
        id: ctx.session.id
      }), Markup.hideKeyboard().extra()).then(() => {
        return ctx.telegram.sendMessage(-1001093968124, ctx.i18n.t('order', {
          data: ctx.session
        }), Markup.inlineKeyboard([{
          text: 'Оплачено',
          callback_data: `ok|${ctx.from.id}|${ctx.session.wallet}|${ctx.session.price}|${ctx.session.id}`
        }]).extra())
      })
    }
  }
)

flow.action(/.+/, (ctx) => {
  const i = ctx.match[0].split('|')
  const data = {
    uid: i[1],
    wallet: i[2],
    price: i[3],
    id: i[4]
  }
  return ctx.telegram.sendMessage(data.uid, ctx.i18n.t('notify', {data: data}))
    .then(ctx.answerCallbackQuery(`Уведомление об оплате счета №${data.id} отправлено.`))
    .then(ctx.editMessageReplyMarkup())
})

flow.register(wizard)


bot.use(flow.middleware())


bot.telegram.getMe().then((botInfo) => {
  bot.options.username = botInfo.username
  console.log('@' + botInfo.username + ' was start.')
})

bot.catch((err) => {
  console.log('Ooops', err)
})

bot.startPolling(30)