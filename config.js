const fs = require('fs')
const yaml = require('js-yaml')

class Config {
  constructor () {
    this.config = yaml.safeLoad(fs.readFileSync('./config.yml'))
    return this.config[process.env.ENV]
  }
}

module.exports = Config