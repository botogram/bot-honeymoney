const fs = require('fs')
const yaml = require('js-yaml')
const path = require('path')

class Info {
  constructor (config) {
    this.config = Object.assign({}, config)

    if (!this.config.directory || !fs.existsSync(this.config.directory)) {
      throw new Error(`Info directory '${this.config.directory}' not found`)
    }
    
    this.upload()
  }
  
  upload () {
    const info = {}
    const files = fs.readdirSync(this.config.directory)
    files.forEach((fileName) => {
      if (path.extname(fileName) === '.yml') {
        const data = fs.readFileSync(path.resolve(this.config.directory, fileName), 'utf8')
        info[path.basename(fileName, '.yml')] = yaml.safeLoad(data)
      } else if (path.extname(fileName) === '.yaml') {
        const data = fs.readFileSync(path.resolve(this.config.directory, fileName), 'utf8')
        info[path.basename(fileName, '.yaml')] = yaml.safeLoad(data)
      } else if (path.extname(fileName) === '.json') {
        info[path.basename(fileName, '.json')] = require(path.resolve(this.config.directory, fileName))
      }
    })
    this.info = info
  }

  middleware() {
    return ((ctx, next) => {
      ctx.info = this.info
      return next()
    })
  }
}

module.exports = Info